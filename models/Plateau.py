import random

from models import Joueur, Carte, President


class Plateau:
    """
    Classe du plateau
    """

    def __init__(self, joueurs: [Joueur]):
        self.cartes = Carte.generation_cartes()
        self.joueurs: [Joueur] = joueurs

        self.jeu_courant : President = President()
        self._cartes_retires: [Carte] = []
        self._distribue()

    def jeu(self):
        while len(self._cartes_retires) != 52:
            for joueur in self.joueurs:
                print("combo", self.jeu_courant.combo_valeurs)
                print("tour saute", self.jeu_courant.count_tour_saute)
                if self.jeu_courant.derniere_carte:
                    print("Dernière carte", self.jeu_courant.derniere_carte.valeur)
                if self.jeu_courant.combo_valeurs == 4:
                    # Carré, fermeture jeu
                    self.jeu_courant.jeu = []
                if self.jeu_courant.count_tour_saute == len(self.joueurs) - 1:
                    # Si plus personne ne veut jouer
                    self.jeu_courant.jeu = []
                print("Dernière carte jouée %s" % self.jeu_courant.derniere_carte)
                cartes = joueur.selectionne_carte(self.jeu_courant)

                if not self.jeu_courant or self.jeu_courant.nbr_cartes == 4:
                    self.jeu_courant = President(len(cartes))
                    print(self.jeu_courant.nbr_cartes)



                if not len(cartes):
                    # Il saut son tour
                    self.jeu_courant.tour_saute = True
                    self.jeu_courant.count_tour_saute += 1
                    print("Saute son tour")
                elif self.jeu_courant.est_move_valide(cartes):
                    print("OK")

                    self.jeu_courant.tour_saute = False
                    self.jeu_courant.count_tour_saute = 0

                    self.jeu_courant.jeu += cartes
                    self._cartes_retires += cartes
                    joueur.retire_cartes(cartes)
                    while self.jeu_courant.derniere_carte.valeur == 13:
                        # Fermeture
                        print("Fermé")
                        self.jeu_courant.jeu = []
                        self.jeu_courant.nbr_cartes = 4
                        cartes = joueur.selectionne_carte(self.jeu_courant)
                        self.jeu_courant.jeu += cartes
                        self._cartes_retires += cartes
                        joueur.retire_cartes(cartes)
                else:
                    self.jeu_courant.tour_saute = True
                    self.jeu_courant.count_tour_saute += 1
                    print("KO")
            print(len(self._cartes_retires))

    def _distribue(self):
        i = 0
        while len(self.cartes):
            # Distribution des cartes
            if i == len(self.joueurs):
                i = 0

            carte = random.choice(self.cartes)
            self.joueurs[i].cartes.append(carte)
            self.cartes.pop(self.cartes.index(carte))
            i += 1

    def pose_carte(self, cartes: [Carte], joueur: Joueur):
        pass
