from models import Carte


class President:
    def __init__(self, nbr_cartes: int = 4):
        self.jeu: [Carte] = []

        if nbr_cartes < 1 or nbr_cartes > 4:
            Exception("Mauvais nombre de cartes")
        # Nombre de cartes à poser en simultanée sur le jeux en cours
        self.nbr_cartes: int = nbr_cartes
        self.tour_saute: bool = False
        self.count_tour_saute: int = 0

        self.combo_valeurs: int = 0
        self.valeur_combo: int = 0

    def est_move_valide(self, cartes) -> bool:
        ret = False
        if len(cartes) == self.nbr_cartes:
            carte_a_poser: Carte = cartes[0]
            if len(self.jeu):
                carte_n_moins_1: Carte = self.jeu[len(self.jeu) - 1]
                if len(self.jeu) > 1:
                    carte_n_moins_2: Carte = self.jeu[len(self.jeu) - 2]
                    if not self.tour_saute and carte_n_moins_1.valeur == carte_n_moins_2.valeur and carte_n_moins_1.valeur != carte_a_poser.valeur:
                        # Si n'est pas x ou rien
                        return False

                if carte_n_moins_1.valeur > carte_a_poser.valeur:
                    # Si carte à poser plus petite
                    return False
            self.tour_saute = False
            self.count_tour_saute: int = 0
            if self.valeur_combo != carte_a_poser.valeur:
                self.combo_valeurs = 0
            else:
                self.combo_valeurs += 1
            return True

        elif not len(self.jeu):
            return True
        return ret

    @property
    def derniere_carte(self) -> Carte or None:
        if len(self.jeu):
            return self.jeu[len(self.jeu)-1]
        return None
