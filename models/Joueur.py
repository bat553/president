import copy

from models import Carte, President


class Joueur:
    """
    Classe des joueurs
    """

    def __init__(self, nom: str = "Joueur"):
        self.cartes: [Carte] = []
        self.nom: str = nom

    def __str__(self):
        return "%s" % self.nom

    @staticmethod
    def generation_joueurs(nombre_joueurs: int) -> []:
        joueurs: [Joueur] = []
        # Génération de objets joueurs
        for i in range(nombre_joueurs):
            joueurs.append(Joueur(f"Joueur {i}"))
        return joueurs

    def _affiche_choix(self, cartes, cartes_selectionnes) -> list:
        i = 0
        restant: [Carte] = []
        string = "%s\n" % self.nom
        for carte in cartes:
            if len(cartes_selectionnes):
                if carte not in cartes_selectionnes and carte.est_cumulable_avec(cartes_selectionnes[0]):
                    restant.append(carte)
                    string += "[%s] (%s)" % (i, str(carte))
                    if i % 4 == 0 and i != 0:
                        string += "\n"
                    else:
                        string += " | "
                    i += 1
            else:
                restant.append(carte)
                string += "[%s] (%s)" % (i, str(carte))
                if i % 4 == 0 and i != 0:
                    string += "\n"
                else:
                    string += " | "
                i += 1
        print(string)
        return restant

    def selectionne_carte(self, jeu: President) -> []:
        # Joue une ou plusieurs cartes
        cartes: [Carte] = copy.copy(self.cartes)
        cartes_selectionnes: [Carte] = []
        index_carte = True
        while True:
            cartes = self._affiche_choix(cartes, cartes_selectionnes)

            if not len(cartes) or len(cartes_selectionnes) == jeu.nbr_cartes or index_carte is None:
                break
            index_carte = input("Quelle carte choisissez vous ? ")

            index_carte = int(index_carte) if index_carte else None
            if index_carte is not None and index_carte < len(cartes):
                cartes_selectionnes.append(cartes[index_carte])

        return cartes_selectionnes

    def retire_cartes(self, cartes: [Carte]) -> None:
        for carte in cartes:
            carte_index = self.cartes.index(carte)
            self.cartes.pop(carte_index)
