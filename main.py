from models import Joueur, Plateau


def main():
    # nombre_joueurs = int(input("Combien de joueurs ? : "))

    nombre_joueurs = 4
    if nombre_joueurs < 2 or nombre_joueurs > 6:
        raise Exception("Nombre de joueur doit être compris entre 3 et 6")
    joueurs: [Joueur] = Joueur.generation_joueurs(nombre_joueurs)

    plateau = Plateau(joueurs)

    plateau.jeu()


if __name__ == "__main__":
    main()
