class Carte:
    """
    Classe des cartes du jeu
    """

    def __init__(self, valeur, couleur, label):
        if couleur not in ["trefle", "carreau", "coeur", "pic"]:
            raise Exception("Couleur non valide")
        if valeur < 1 or valeur > 13:
            raise Exception("Valeur non valide")

        self.couleur: str = couleur
        self.valeur: int = valeur
        self.label: int = label
        self.score: int = 0

    @staticmethod
    def generation_cartes() -> []:
        cartes: [Carte] = []
        # Génération des 52 cartes
        for couleur in ["trefle", "carreau", "coeur", "pic"]:
            for label in range(1, 14):
                if label == 1:
                    valeur = 12
                elif label == 2:
                    valeur = 13
                else:
                    valeur = label-2
                cartes.append(Carte(valeur, couleur, label))
        return cartes

    def est_cumulable_avec(self, carte) -> bool:
        if self.valeur == carte.valeur:
            return True
        return False

    def __str__(self):
        if self.label == 1:
            return "%s de %s" % ("As", self.couleur.capitalize())
        elif 1 < self.label < 11:
            return "%s de %s" % (self.label, self.couleur.capitalize())
        elif self.label == 11:
            return "%s de %s" % ("Valet", self.couleur.capitalize())
        elif self.label == 12:
            return "%s de %s" % ("Dame", self.couleur.capitalize())
        elif self.label == 13:
            return "%s de %s" % ("Roi", self.couleur.capitalize())
        else:
            raise Exception("Value invalid")
